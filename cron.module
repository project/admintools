<?php

/**
 * @file
 * List drupal sites for being visted with cron.
 */

/**
 * Implementation of hook_perm().
 */
function cron_perm() {
  return array('admin tools');
}

/**
 * Implementation of hook_menu().
 */
function cron_menu() {
  $items = array();

  $items['admin/settings/tools/cron'] = array(
    'title' => 'Cron',
    'description' => 'Administer all your crons from one drupal',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cron_list_form'),
    'access arguments' => array('admin tools'),
  );
  $items['admin/settings/tools/cron/list'] = array(
    'title' => 'List',
    'weight' => 1,
    'type' => MENU_DEFAULT_LOCAL_TASK
  );
  $items['admin/settings/tools/cron/add'] = array(
    'title' => 'Add',
    'description' => 'Administer all your crons from one drupal',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cron_add_form'),
    'access arguments' => array('admin tools'),
    'weight' => 2,
    'type' => MENU_LOCAL_TASK
  );
  $items['admin/settings/tools/cron/edit/%'] = array(
    'title' => 'Edit',
    'description' => 'Administer all your crons from one drupal',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cron_edit_form', 5),
    'access arguments' => array('admin tools'),
    'type' => MENU_CALLBACK
  );
  $items['admin/settings/tools/cron/delete/%'] = array(
    'title' => 'Edit',
    'description' => 'Administer all your crons from one drupal',
    'page callback' => 'cron_delete',
    'page arguments' => array(5),
    'access arguments' => array('admin tools'),
    'type' => MENU_CALLBACK
  );
  $items['admin/settings/tools/cron/settings'] = array(
    'title' => 'Settings',
    'description' => 'Basic settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cron_settings_form'),
    'access arguments' => array('admin tools'),
    'weight' => 3,
    'type' => MENU_LOCAL_TASK
  );
  return $items;
}

function cron_list_form() {

  $header = array(
    t('Site'),
    t('Cron key (Drupal 7)'),
    t('Description'),
    array('data' => t('Checks'), 'field' => 'checks'),
    t('Last check'),
    array('data' => t('Errors'), 'field' => 'errors'),
    t('Last error'),
    array('data' => t('Disabled'), 'field' => 'disabled')
  );

  $sql = 'SELECT DISTINCT sid, url, cron_key, description, checks, errors, last_check, last_error, disabled FROM {cron}';
  $sql .= tablesort_sql($header);
  $query_count = 'SELECT COUNT(DISTINCT url) FROM {cron}';
  $result = pager_query($sql, 50, 0, $query_count);

  $sites = array();
  while ($site = db_fetch_object($result)) {
    $sites[$site->sid] = '';
    $form['url'][$site->sid] = array('#value' => $site->url);
    $form['cron_key'][$site->sid] = array('#value' => $site->cron_key);
    $form['description'][$site->sid] = array('#value' => $site->description);
    $form['checks'][$site->sid] = array('#value' => $site->checks);
    $form['last_check'][$site->sid] = array('#value' => format_interval(time() - $site->last_check));
    $form['errors'][$site->sid] =  array('#value' => $site->errors);
    $form['last_error'][$site->sid] = array('#value' => ($site->last_error == 0) ? 'N/A' : format_interval(time() - $site->last_error));
    $form['disabled'][$site->sid] = array('#type' => 'checkbox', '#default_value' => $site->disabled);
    $form['operations'][$site->sid] = array('#value' => l(t('edit'), "admin/settings/tools/cron/edit/$site->sid") .' '. l(t('delete'), "admin/settings/tools/cron/delete/$site->sid"));
  }
  $form['sites'] = array(
    '#type' => 'checkboxes',
    '#options' => $sites
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));

  return $form;
}

function cron_add_form() {

  $form['notes'] = array(
    '#value' => t('Site: Drupal URL. <br />Description: A description for your site. <br /> Cron_key: The cron key required for Drupal 7')
  );
  $form['url'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#required' => TRUE,
  );
  $form['cron_key'] =  array(
    '#type' => 'textfield',
    '#size' => 20,
  );
  $form['description'] =  array(
    '#type' => 'textfield',
    '#size' => 20,
  );
  $form['interval'] =  array(
    '#type' => 'textfield',
    '#size' => 3,
  );
  $form['operations']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#weight' => 1
  );
  $form['operations']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#weight' => 2
  );
  return $form;
}

function cron_add_form_validate($form, &$form_state) {

  switch ($form_state['values']['op']) {
    case t('Cancel'):
      drupal_goto('admin/settings/tools/cron');
      break;
    case t('Add'):
    default:
      $url = drupal_strtolower(trim($form_state['values']['url']));
      if (drupal_substr($url, drupal_strlen($url) - 8) == 'cron.php') {
        $url = drupal_substr($url, 0, -9);
      }
      if (strrpos($url, '/') == drupal_strlen($url) - 1) {
        $url = drupal_substr($url, 0, -1);
      }
      if (!((strstr($url, 'http://')) || (strstr($url, 'https://')) )) {
        $url = 'http://'. $url;
      }

      $row = db_result(db_query("SELECT sid FROM {cron} WHERE url LIKE LOWER('%s')", $url));
      if ($row) {
        form_set_error('url', t('This address is already in your list.'));
      }

      $form_state['values']['url'] = $url;
      $url .= '/cron.php';
      if(isset($form_state['values']['cron_key'])) {
        $url .= '?cron_key='.$form_state['values']['cron_key'];
      }

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HEADER, TRUE);
      curl_setopt($ch, CURLOPT_NOBODY, TRUE);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_exec($ch);
      $result = curl_getinfo($ch);
      if ($result['http_code'] != '200') {
        form_set_error('url', t('Your cron is not reacheable at the given address: @url', array('@url' => $url)));
      }

      if ( (!empty($form_state['values']['interval'])) && (!is_numeric($form_state['values']['interval']))) {
        form_set_error('interval', t('The interval setting has to be a number.'));
      }
  }

  return;
}

function cron_add_form_submit($form, $form_state) {
  db_query("INSERT INTO {cron} (url, cron_key, description, checks, last_check, intervals, count) VALUES ('%s', '%s', '%s', 1, '%d', '%d', '%d')", $form_state['values']['url'], $form_state['values']['cron_key'], $form_state['values']['description'], time(), $form_state['values']['interval'], $form_state['values']['interval']);
  drupal_goto('admin/settings/tools/cron');
}

function cron_edit_form($form_state, $sid = 0) {
  $sql = db_query("SELECT url, cron_key, description, checks, errors, intervals FROM {cron} WHERE sid = '%d'", $sid);
  $row = db_fetch_object($sql);
  $form['sid'] = array(
    '#type' => 'hidden',
    '#default_value' => $sid,
  );
  $form['checks'] = array(
    '#type' => 'hidden',
    '#default_value' => $row->checks,
  );
  $form['url'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => $row->url,
  );
  $form['cron_key'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => $row->cron_key,
  );
  $form['description'] =  array(
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => $row->description,
  );
  $form['interval'] =  array(
    '#type' => 'textfield',
    '#size' => 3,
    '#default_value' => $row->intervals,
  );
  $form['operations']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 1
  );
  $form['operations']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#weight' => 2
  );

  return $form;
}

function cron_edit_form_validate($form, &$form_state) {

  switch ($form_state['values']['op']) {
    case t('Cancel'):
      drupal_goto('admin/settings/tools/cron');
      break;
    case t('Save'):
    default:
      $url = drupal_strtolower(trim($form_state['values']['url']));
      if (drupal_substr($url, drupal_strlen($url) - 8) == 'cron.php') {
        $url = drupal_substr($url, 0, -9);
      }
      if (strrpos($url, '/') == drupal_strlen($url) - 1) {
        $url = drupal_substr($url, 0, -1);
      }
      if (!((strstr($url, 'http://')) || (strstr($url, 'https://')) )) {
        $url = 'http://'. $url;
      }

      $form_state['values']['url'] = $url;
      $url .= '/cron.php';
      if(isset($form_state['values']['cron_key'])) {
        $url .= '?cron_key='.$form_state['values']['cron_key'];
      }
      
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HEADER, TRUE);
      curl_setopt($ch, CURLOPT_NOBODY, TRUE);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_exec($ch);
      $result = curl_getinfo($ch);
      if ($result['http_code'] != '200') {
        form_set_error('url', t('Your cron is not reacheable at the given address: @url', array('@url' => $url)));
      }
      
      if ( (!empty($form_state['values']['interval'])) && (!is_numeric($form_state['values']['interval']))) {
        form_set_error('interval', t('The interval setting has to be a number.'));
      }
  }

  return;
}

function cron_edit_form_submit($form, $form_state) {
  db_query("UPDATE {cron} SET url = '%s', cron_key = '%s', description = '%s', checks = '%d', last_check = '%d', disabled = 'FALSE', intervals = '%d', count = '%d' WHERE sid = '%d'", $form_state['values']['url'], $form_state['values']['cron_key'], $form_state['values']['description'], $form_state['values']['checks'] + 1, time(), $form_state['values']['sid'], $form_state['values']['interval'], $form_state['values']['interval']);
  drupal_goto('admin/settings/tools/cron');
}

function cron_delete($sid = 0) {
  db_query("DELETE FROM {cron} WHERE sid = '%d'", $sid);
  drupal_goto('admin/settings/tools/cron');
}

function cron_settings_form() {

  $form['reset'] = array(
    '#type' => 'fieldset',
    '#description' => t('Reset cron interval timer'),
    '#title' => 'Reset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['reset']['button'] = array(
    '#type' => 'submit',
    '#description' => t('Reset cron interval timer'),
    '#value' => t('Reset'),
    '#submit' => 'cron_interval_reset'
  );
  $form['fault_level'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#title' => t('Fault level'),
    '#description' => t('Configure the fault level on which a site has to be disabled'),
    '#default_value' => variable_get('fault_level', 0)
  );

  $form['#validate'] = array('cron_interval_validate');
  return system_settings_form($form);
}

/**
 * Validate callback.
 */
function cron_interval_validate($form, &$form_state) {
  if ($form_state['values']['op'] == t('Reset')) {
    variable_set('cron_interval_start', 0);
    variable_set('cron_interval_end', 0);
    drupal_goto('admin/settings/tools/cron/settings');
  }
}
  
/**
 * Implementation of hook_help().
 */
function cron_help($path, $arg) {

  switch ($path) {
    case 'admin/settings/tools/cron':
      if (($end = variable_get('cron_interval_end', 0)) && ($start = variable_get('cron_interval_start', 0))) {
        return '<p>'. t('The cron of this site runs every '.format_interval($end - $start)) .'</p>';
      }
  }
}

/**
 * Implementation of hook_theme().
 */
function cron_theme() {
  return array(
    'cron_list_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'cron_add_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'cron_edit_form' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Theme sites overview.
 *
 * @ingroup themeable
 */
function theme_cron_list_form($form) {
  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    t('Site'),
    t('Cron key (Drupal 7)'),
    t('Description'),
    array('data' => t('Checks'), 'field' => 'checks'),
    t('Last check'),
    array('data' => t('Errors'), 'field' => 'errors'),
    t('Last error'),
    array('data' => t('Disabled'), 'field' => 'disabled'),
    t('Operations')
  );
  if (isset($form['url']) && is_array($form['url'])) {
    foreach (element_children($form['url']) as $key) {
      $rows[] = array(
        drupal_render($form['sites'][$key]),
        drupal_render($form['url'][$key]),
        drupal_render($form['cron_key'][$key]),
        drupal_render($form['description'][$key]),
        drupal_render($form['checks'][$key]),
        drupal_render($form['last_check'][$key]),
        drupal_render($form['errors'][$key]),
        drupal_render($form['last_error'][$key]),
        drupal_render($form['disabled'][$key]),
        drupal_render($form['operations'][$key]),
      );
    }
  }
  else {
    $rows[] = array(array('data' => t('No sites available.'), 'colspan' => '10'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Theme add stie.
 *
 * @ingroup themeable
 */
function theme_cron_add_form($form) {
  // Overview table:
  $header = array(
    t('Site'),
    t('Cron key (Drupal 7)'),
    t('Description'),
    t('Interval'),
    t('Operations')
  );

  if (isset($form['url']) && is_array($form['url'])) {
    $rows[] = array(
      drupal_render($form['url']),
      drupal_render($form['cron_key']),
      drupal_render($form['description']),
      drupal_render($form['interval']),
      drupal_render($form['operations']),
    );
  }
  else {
    $rows[] = array(array('data' => t('No sites available.'), 'colspan' => '7'));
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme edit site.
 *
 * @ingroup themeable
 */
function theme_cron_edit_form($form) {
  // Overview table:
  $header = array(
    t('Site'),
    t('Cron key (Drupal 7)'),
    t('Description'),
    t('Interval'),
    t('Operations')
  );
  if (isset($form['url']) && is_array($form['url'])) {
    $rows[] = array(
      drupal_render($form['url']),
      drupal_render($form['cron_key']),
      drupal_render($form['description']),
      drupal_render($form['interval']),
      drupal_render($form['operations']),
    );
  }
  else {
    $rows[] = array(array('data' => t('No sites available.'), 'colspan' => '7'));
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

/**
 * Implementation of hook_cron().
 */
function cron_cron() {

  if (variable_get('cron_interval_start', 0) == 0) {
    variable_set('cron_interval_start', time());
  }
  else {
    if (variable_get('cron_interval_end', 0) == 0) {
      variable_set('cron_interval_end', time());
    }
  }

  $sql = db_query("SELECT sid, url, cron_key, checks, errors, last_check, last_error, intervals, count FROM {cron} WHERE disabled = '%d'", FALSE);
  while ($row = db_fetch_object($sql)) {
    if ($row->count == 0) {
      $checks = $row->checks + 1;
      $errors = $row->errors ;
      $last_error = $row->last_error;
      $candidate = FALSE;
      if ($row->last_check == $row->last_error) {
        $candidate = TRUE;
      }

      if (function_exists('curl_init')) {

        $url = $row->url .'/cron.php';
        if (isset($row->cron_key)) {
          $url .= '?cron_key=' . $row->cron_key;
        }

        $options = array(
          CURLOPT_RETURNTRANSFER => TRUE,         // return web page
          CURLOPT_HEADER         => TRUE,        // don't return headers
          CURLOPT_NOBODY         => TRUE,
          CURLOPT_FOLLOWLOCATION => FALSE,         // don't follow redirects
          CURLOPT_USERAGENT      => 'spider',     // who am i
          CURLOPT_AUTOREFERER    => TRUE,         // set referer on redirect
          CURLOPT_CUSTOMREQUEST  => 'HEAD',
          CURLOPT_CONNECTTIMEOUT => 60,          // timeout on connect
          CURLOPT_TIMEOUT        => 60,          // timeout on response
          CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch,$options); 
        curl_exec($ch);

        $result = curl_getinfo($ch);
        $checks = $row->checks + 1;
        $errors = $row->errors ;
        $last_error = $row->last_error;
        if ($result['http_code'] != '200') {
          $errors += 1;
          $last_error = time();
        }
        curl_close($ch);
      }
      else { 

        if (strstr($url, 'https://')) {
          $port = 443;
          $url = substr($row->url, 8);
        } 
        else {
          $port = 80;
          $url = substr($row->url, 7);
        }
        if (strpos($url, '/')) {
           $path = substr($url, strpos($url, '/'));
          $url = substr($url, 0, strpos($url, '/'));
        }
        $fp = fsockopen($url, $port, $errno, $errstr, 30);
        $page = $path ."/cron.php";
        $out="GET $page HTTP/1.1\r\n"; 
        $out.="Host: $url\r\n"; 
        $out.="Connection: Close\r\n\r\n";
        fwrite($fp,$out);
        $content=fgets($fp);
        $result = explode(' ', $content);
        if ($result[1] != '200') {
          $errors += 1;
          $last_error = time();
        }
        fclose($fp);
      }

      $disable = FALSE;
      $level = variable_get('fault_level', 0) / 100.0;
      if (($last_error > $row->last_error) && ($candidate == TRUE) && ( $errors > ($level * $checks) )) {
        // when 2 last checks failed and 90% of all checks failed, disable this site
        $disable = TRUE;
      }
      db_query("UPDATE {cron} SET checks = '%d', last_check = '%d', errors = '%d', last_error = '%d', disabled = '%d', count = '%d' WHERE sid = '%d'", $checks, time(), $errors, $last_error, $disable, $row->intervals, $row->sid);
    }
    else {
      $row->count--;
      db_query("UPDATE {cron} SET count = '%d' WHERE sid = '%d'", $row->count, $row->sid);
    }
  }
}
